# Estimating pi (π) with Monte Carlo.
This is for the Uebung.

    go build .

Is all one needs.

Cross compiling for windows...

   GOOS=windows GOARCH=amd64 go build .
   
# BUGS

There have been some terrible bugs in gonum/plot - mostly to do with large numbers of points. I am not going to fix them. Since seeing this, I have swapped to using a different chart package.

